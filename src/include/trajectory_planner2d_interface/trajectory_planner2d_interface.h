
#ifndef _TRAJECTORY_PLANNER2D_INTERFACE_H_
#define _TRAJECTORY_PLANNER2D_INTERFACE_H_



#include <string>


#include <Eigen/Dense>


#include <ros/ros.h>

#include <tf/transform_datatypes.h>


//// TrajectoryPlanner2dInterfacePoseIn

#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <droneMsgsROS/dronePose.h>




//// MissionPointIn

// TODO
#include <geometry_msgs/PoseStamped.h>

#include <droneMsgsROS/dronePositionRefCommand.h>



//// SocietyPoseIn

// TODO



//// ObstaclesIn

// TODO
//#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <droneMsgsROS/obstaclesTwoDim.h>

#include <obstacle2d_converter_core/obstacle2d_converter.h>




//// PlannedTrajectoryOut

#include <nav_msgs/Path.h>

#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>






///////////////////////////////////////
/// \brief The TrajectoryPlanner2dInterfacePoseIn class
////////////////////////////////////
class TrajectoryPlanner2dInterfacePoseIn
{
public:
    TrajectoryPlanner2dInterfacePoseIn(int argc,char **argv);


protected:
    ros::NodeHandle* nh_;


protected:
    std::string pose_in_sub_topic_name_;
    ros::Subscriber pose_in_sub_;
    void poseInCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
protected:
    std::string pose_in_pub_topic_name_;
    ros::Publisher pose_in_pub_;
    droneMsgsROS::dronePose pose_in_pub_msg_;

protected:
    void readParameters();

public:
    int open();


public:
    int run();



};







class ObstacleAssociation
{
public:
    ~ObstacleAssociation();

public:
    int id_;
    Obstacle3d* obstacle3d_;
    Obstacle2d* obstacle2d_;

};




///////////////////////////////////////
/// \brief The TrajectoryPlanner2dInterfaceObstaclesIn class
////////////////////////////////////
class TrajectoryPlanner2dInterfaceObstaclesIn
{
public:
    TrajectoryPlanner2dInterfaceObstaclesIn(int argc,char **argv);
    ~TrajectoryPlanner2dInterfaceObstaclesIn();


protected:
    ros::NodeHandle* nh_;

protected:
    Plane3d plane3d_;
    std::list<ObstacleAssociation*> list_obstacle_;

protected:
    std::string pose_in_sub_topic_name_;
    ros::Subscriber pose_in_sub_;
    void poseInCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
protected:
    std::string obstacles_in_sub_topic_name_;
    ros::Subscriber obstacles_in_sub_;
    void obstaclesInCallback(const visualization_msgs::MarkerArray::ConstPtr &msg);
protected:
    std::string obstacles_in_pub_topic_name_;
    ros::Publisher obstacles_in_pub_;
    droneMsgsROS::obstaclesTwoDim obstacles_in_pub_msg_;

protected:
    void readParameters();

public:
    int open();


public:
    int run();

protected:
    int publish();



};







///////////////////////////////////////
/// \brief The TrajectoryPlanner2dInterfacePlannedTrajectoryOut class
////////////////////////////////////
class TrajectoryPlanner2dInterfaceTrajectoryOut
{
public:
    TrajectoryPlanner2dInterfaceTrajectoryOut(int argc,char **argv);


protected:
    ros::NodeHandle* nh_;


protected:
    std::string planned_trajectory_out_sub_topic_name_;
    ros::Subscriber planned_trajectory_out_sub_;
    void plannedTrajectoryOutCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg);
protected:
    std::string planned_trajectory_out_pub_topic_name_;
    ros::Publisher planned_trajectory_out_pub_;
    nav_msgs::Path planned_trajectory_out_pub_msg_;

protected:
    void readParameters();

public:
    int open();


public:
    int run();



};








#endif
