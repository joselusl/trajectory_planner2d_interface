

//I/O stream
//std::cout
#include <iostream>


//
#include "trajectory_planner2d_interface/trajectory_planner2d_interface.h"



int main(int argc,char **argv)
{
    TrajectoryPlanner2dInterfaceObstaclesIn trajectory_planner2d_interface_obstacles_in(argc, argv);

    trajectory_planner2d_interface_obstacles_in.open();

    trajectory_planner2d_interface_obstacles_in.run();

    return 0;
}
