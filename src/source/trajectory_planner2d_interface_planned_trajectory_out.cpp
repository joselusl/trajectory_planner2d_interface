

//I/O stream
//std::cout
#include <iostream>


//
#include "trajectory_planner2d_interface/trajectory_planner2d_interface.h"



int main(int argc,char **argv)
{
    TrajectoryPlanner2dInterfaceTrajectoryOut trajectory_planner2d_interface_planned_trajectory_out(argc, argv);

    trajectory_planner2d_interface_planned_trajectory_out.open();

    trajectory_planner2d_interface_planned_trajectory_out.run();

    return 0;
}
