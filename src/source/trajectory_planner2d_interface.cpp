
#include "trajectory_planner2d_interface/trajectory_planner2d_interface.h"


TrajectoryPlanner2dInterfacePoseIn::TrajectoryPlanner2dInterfacePoseIn(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // end
    return;
}

void TrajectoryPlanner2dInterfacePoseIn::readParameters()
{
    //
    ros::param::param<std::string>("~pose_in_sub_topic_name", pose_in_sub_topic_name_, "msf_localization/robot_pose");
    ROS_INFO("pose_in_sub_topic_name = %s",pose_in_sub_topic_name_.c_str());
    //
    ros::param::param<std::string>("~pose_in_pub_topic_name", pose_in_pub_topic_name_, "drone0/EstimatedPose_droneGMR_wrt_GFF");
    ROS_INFO("pose_in_pub_topic_name = %s",pose_in_pub_topic_name_.c_str());

    // end
    return;
}

int TrajectoryPlanner2dInterfacePoseIn::open()
{
    // Subscribers
    pose_in_sub_ = nh_->subscribe(pose_in_sub_topic_name_, 1, &TrajectoryPlanner2dInterfacePoseIn::poseInCallback, this);

    // Publishers
    pose_in_pub_ = nh_->advertise<droneMsgsROS::dronePose>(pose_in_pub_topic_name_, 10);

    // end
    return 0;
}

int TrajectoryPlanner2dInterfacePoseIn::run()
{
    ros::spin();

    return 0;
}

void TrajectoryPlanner2dInterfacePoseIn::poseInCallback(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    // Read geometry_msgs/poseStamped
    // Nothing to do

    // Transform to droneMsgsROS/dronePose
    // Header
    pose_in_pub_msg_.header=msg->header;
    pose_in_pub_msg_.time=msg->header.stamp.toSec(); // Deprecated
    // Pose
    pose_in_pub_msg_.x=msg->pose.position.x;
    pose_in_pub_msg_.y=msg->pose.position.y;
    pose_in_pub_msg_.z=msg->pose.position.z;
    tf::Quaternion q(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z, msg->pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    pose_in_pub_msg_.roll=roll;
    pose_in_pub_msg_.pitch=pitch;
    pose_in_pub_msg_.yaw=yaw;
    // reference frames
    // TODO check
    pose_in_pub_msg_.reference_frame="world";
    pose_in_pub_msg_.target_frame="robot";
    // orientation system
    pose_in_pub_msg_.YPR_system="wYvPuR";

    // publish
    pose_in_pub_.publish(pose_in_pub_msg_);

    // end
    return;
}





ObstacleAssociation::~ObstacleAssociation()
{
    delete obstacle3d_;
    delete obstacle2d_;

    return;
}






TrajectoryPlanner2dInterfaceObstaclesIn::TrajectoryPlanner2dInterfaceObstaclesIn(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // end
    return;
}

TrajectoryPlanner2dInterfaceObstaclesIn::~TrajectoryPlanner2dInterfaceObstaclesIn()
{
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        delete (*it_list_obstacle);
    }
    list_obstacle_.clear();

    return;
}

void TrajectoryPlanner2dInterfaceObstaclesIn::readParameters()
{
    //
    ros::param::param<std::string>("~pose_in_sub_topic_name", pose_in_sub_topic_name_, "msf_localization/robot_pose");
    ROS_INFO("pose_in_sub_topic_name = %s",pose_in_sub_topic_name_.c_str());
    //
    ros::param::param<std::string>("~obstacles_in_sub_topic_name", obstacles_in_sub_topic_name_, "obstacles");
    ROS_INFO("obstacles_in_sub_topic_name = %s",obstacles_in_sub_topic_name_.c_str());
    //
    ros::param::param<std::string>("~obstacles_in_pub_topic_name", obstacles_in_pub_topic_name_, "drone0/obstacles");
    ROS_INFO("obstacles_in_pub_topic_name = %s",obstacles_in_pub_topic_name_.c_str());

    // end
    return;
}

int TrajectoryPlanner2dInterfaceObstaclesIn::open()
{
    // Subscribers
    pose_in_sub_ = nh_->subscribe(pose_in_sub_topic_name_, 1, &TrajectoryPlanner2dInterfaceObstaclesIn::poseInCallback, this);
    obstacles_in_sub_ = nh_->subscribe(obstacles_in_sub_topic_name_, 1, &TrajectoryPlanner2dInterfaceObstaclesIn::obstaclesInCallback, this);

    // Publishers
    obstacles_in_pub_ = nh_->advertise<droneMsgsROS::obstaclesTwoDim>(obstacles_in_pub_topic_name_, 10);

    // end
    return 0;
}

int TrajectoryPlanner2dInterfaceObstaclesIn::run()
{
    ros::spin();

    return 0;
}

void TrajectoryPlanner2dInterfaceObstaclesIn::poseInCallback(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    // Update plane3d

    // Horizontal plane
    Eigen::Vector3d normal(0, 0, 1);
    double distance=-msg->pose.position.z;

    // Set
    plane3d_.setPlane3d(normal, distance);

    // prepare message time
    obstacles_in_pub_msg_.time=msg->header.stamp.toSec();

    // publish
    publish();

    // end
    return;
}

void TrajectoryPlanner2dInterfaceObstaclesIn::obstaclesInCallback(const visualization_msgs::MarkerArray::ConstPtr &msg)
{
    // check
    if(msg->markers.empty())
        return;


    // Update list of obstacles3d
    for(int i=0; i<msg->markers.size(); i++)
    {
        // find obstacle in list by id
        std::list<ObstacleAssociation*>::iterator it_list_obstacle;
        bool found=false;
        for(it_list_obstacle=list_obstacle_.begin();
            it_list_obstacle!=list_obstacle_.end();
            ++it_list_obstacle)
        {
            if((*it_list_obstacle)->id_ == msg->markers[i].id)
            {
                found=true;
                break;
            }
        }

        // create if it does not exist
        if(!found || it_list_obstacle==list_obstacle_.end())
        {
            // ObstacleAssociation
            ObstacleAssociation* obstacle=new ObstacleAssociation();

            // id
            obstacle->id_=msg->markers[i].id;

            // obstacle3d by Type
            switch(msg->markers[i].type)
            {
                case visualization_msgs::Marker::CUBE:
                {
                    Eigen::Vector3d position_cube_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cube_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector3d size(msg->markers[i].scale.x, msg->markers[i].scale.y, msg->markers[i].scale.z);
                    Obstacle3d* obstacle3d=new Cuboid3d(position_cube_wrt_world, attitude_cube_wrt_world, size);
                    obstacle->obstacle3d_=obstacle3d;
                    break;
                }
                default:
                    break;
            }

            // push
            list_obstacle_.push_back(obstacle);
        }
        // update if it exists
        else
        {
            // obstacle3d by Type
            switch(msg->markers[i].type)
            {
                case visualization_msgs::Marker::CUBE:
                {
                    Cuboid3d* cuboid3d=static_cast<Cuboid3d*>((*it_list_obstacle)->obstacle3d_);

                    Eigen::Vector3d position_cube_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cube_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector3d size(msg->markers[i].scale.x, msg->markers[i].scale.y, msg->markers[i].scale.z);

                    cuboid3d->setPoseCenterWrtWorld(position_cube_wrt_world, attitude_cube_wrt_world);
                    cuboid3d->setSize(size);

                    break;
                }
                default:
                    break;
            }
        }
    }

    // Prepare message time
    obstacles_in_pub_msg_.time=ros::Time::now().toSec();

    // publish
    publish();

    // end
    return;
}

int TrajectoryPlanner2dInterfaceObstaclesIn::publish()
{
    //ROS_INFO("publishing");

    // Checks
    if(list_obstacle_.empty() || !plane3d_.isOk())
        return -1;

    // Calculate list of obstacles2d
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        int error_convert=Obstacle2dConverter::convert((*it_list_obstacle)->obstacle3d_, plane3d_, (*it_list_obstacle)->obstacle2d_);
        if(error_convert<0)
        {
            // Error
            ROS_INFO("error_convert");
            continue;
        }
        else if(error_convert>0)
        {
            // No intersection
            //ROS_INFO("no intersection");
            continue;
        }
    }

    // prepare message
    //std::cout<<"list_obstacle_ size="<<list_obstacle_.size()<<std::endl;
    obstacles_in_pub_msg_.walls.clear();
    obstacles_in_pub_msg_.poles.clear();
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        // check
        if(!(*it_list_obstacle)->obstacle2d_)
        {
            //ROS_ERROR("no pointer in obstacle2d");
            continue;
        }

        // pose center
        Eigen::Vector2d position_center_wrt_world;
        double attitude_center_wrt_world;
        int error_get_pose_center=(*it_list_obstacle)->obstacle2d_->getPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
        if(error_get_pose_center)
        {
            ROS_ERROR("error_get_pose_center");
            continue;
        }

        // others
        switch((*it_list_obstacle)->obstacle2d_->getObstacle2dType())
        {
            // rectangles
            case Obstacle2dTypes::rectangle2d:
            {
                Rectangle2d* rectangle2d=static_cast<Rectangle2d*>((*it_list_obstacle)->obstacle2d_);

                droneMsgsROS::obstacleTwoDimWall obstacle2d_wall;

                obstacle2d_wall.id=(*it_list_obstacle)->id_;

                obstacle2d_wall.centerX=position_center_wrt_world(0);
                obstacle2d_wall.centerY=position_center_wrt_world(1);

                obstacle2d_wall.yawAngle=attitude_center_wrt_world;

                Eigen::Vector2d size;
                int error_get_size=rectangle2d->getSize(size);
                if(error_get_size)
                {
                    ROS_ERROR("error_get_size");
                    continue;
                }

                obstacle2d_wall.sizeX=size(0);
                obstacle2d_wall.sizeY=size(1);

                obstacle2d_wall.isVirtual=false;

                obstacles_in_pub_msg_.walls.push_back(obstacle2d_wall);

                break;
            }
            // ellipses
            case Obstacle2dTypes::ellipse2d:
            {
                Ellipse2d* ellipse2d=static_cast<Ellipse2d*>((*it_list_obstacle)->obstacle2d_);

                droneMsgsROS::obstacleTwoDimPole obstacle2d_pole;

                obstacle2d_pole.id=(*it_list_obstacle)->id_;

                obstacle2d_pole.centerX=position_center_wrt_world(0);
                obstacle2d_pole.centerY=position_center_wrt_world(1);

                obstacle2d_pole.yawAngle=attitude_center_wrt_world;

                // TODO

                obstacle2d_pole.isVirtual=false;

                obstacles_in_pub_msg_.poles.push_back(obstacle2d_pole);

                break;
            }
            default:
                break;
        }
    }

    // Publish
    obstacles_in_pub_.publish(obstacles_in_pub_msg_);

    // end
    return 0;
}









TrajectoryPlanner2dInterfaceTrajectoryOut::TrajectoryPlanner2dInterfaceTrajectoryOut(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // end
    return;
}

void TrajectoryPlanner2dInterfaceTrajectoryOut::readParameters()
{
    //
    ros::param::param<std::string>("~planned_trajectory_out_sub_topic_name", planned_trajectory_out_sub_topic_name_, "drone0/droneTrajectoryAbsRefCommand");
    ROS_INFO("planned_trajectory_out_sub_topic_name = %s",planned_trajectory_out_sub_topic_name_.c_str());
    //
    ros::param::param<std::string>("~planned_trajectory_out_pub_topic_name", planned_trajectory_out_pub_topic_name_, "planned_trajectory");
    ROS_INFO("planned_trajectory_out_pub_topic_name = %s",planned_trajectory_out_pub_topic_name_.c_str());

    // end
    return;
}

int TrajectoryPlanner2dInterfaceTrajectoryOut::open()
{
    // Subscribers
    planned_trajectory_out_sub_ = nh_->subscribe(planned_trajectory_out_sub_topic_name_, 1, &TrajectoryPlanner2dInterfaceTrajectoryOut::plannedTrajectoryOutCallback, this);

    // Publishers
    planned_trajectory_out_pub_ = nh_->advertise<nav_msgs::Path>(planned_trajectory_out_pub_topic_name_, 10);

    // end
    return 0;
}

int TrajectoryPlanner2dInterfaceTrajectoryOut::run()
{
    ros::spin();

    return 0;
}

void TrajectoryPlanner2dInterfaceTrajectoryOut::plannedTrajectoryOutCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg)
{
    // Read droneMsgsROS::dronePositionTrajectoryRefCommand
    // Nothing to do

    // Transform to nav_msgs::Path
    // Header
    planned_trajectory_out_pub_msg_.header.stamp=msg->header.stamp;
    planned_trajectory_out_pub_msg_.header.frame_id="world";
    // waypoints
    planned_trajectory_out_pub_msg_.poses.clear();
    for(int i=0; i<msg->droneTrajectory.size(); i++)
    {
        // auxiliar waypoint
        geometry_msgs::PoseStamped waypoint;
        // waypoint header: not used
        waypoint.header.stamp=msg->header.stamp;
        waypoint.header.frame_id="world";
        // waypoint attitude: not used
        waypoint.pose.orientation.w=1;
        waypoint.pose.orientation.x=0;
        waypoint.pose.orientation.y=0;
        waypoint.pose.orientation.z=0;
        // waypoint position
        waypoint.pose.position.x=msg->droneTrajectory[i].x;
        waypoint.pose.position.y=msg->droneTrajectory[i].y;
        waypoint.pose.position.z=msg->droneTrajectory[i].z;

        // push back
        planned_trajectory_out_pub_msg_.poses.push_back(waypoint);
    }


    // publish
    planned_trajectory_out_pub_.publish(planned_trajectory_out_pub_msg_);

    // end
    return;
}
