

//I/O stream
//std::cout
#include <iostream>


//
#include "trajectory_planner2d_interface/trajectory_planner2d_interface.h"



int main(int argc,char **argv)
{
    TrajectoryPlanner2dInterfacePoseIn trajectory_planner2d_interface_pose_in(argc, argv);

    trajectory_planner2d_interface_pose_in.open();

    trajectory_planner2d_interface_pose_in.run();

    return 0;
}
